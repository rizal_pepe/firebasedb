package com.example.androidlatihan15firebasedb.page

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log.e
import android.widget.Toast
import com.example.androidlatihan15firebasedb.R
import com.example.androidlatihan15firebasedb.data.Pref
import com.example.androidlatihan15firebasedb.data.adapter.BukuAdapter
import com.example.androidlatihan15firebasedb.data.model.BukuModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.homepage.*

class HomePage : AppCompatActivity(), BukuAdapter.FirebaseDataListener {



    private lateinit var fAuth: FirebaseAuth
    private var aBuku: BukuAdapter? = null
    private var recyclerView: RecyclerView? = null
    private var list: MutableList<BukuModel> = ArrayList<BukuModel>()
    lateinit var dbRef: DatabaseReference
    lateinit var pref: Pref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.homepage)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView!!.layoutManager = LinearLayoutManager(this)
        recyclerView!!.setHasFixedSize(true)

        pref = Pref(this)

        dbRef = FirebaseDatabase.getInstance()
            .getReference("buku/${pref.getUid()}")
        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                list = ArrayList()
                for (dataSnapshot in p0.children) {
                    val addDataAll = dataSnapshot.getValue(
                        BukuModel::class.java
                    )
                    addDataAll!!.setKey(dataSnapshot.key!!)
                    list.add(addDataAll)
                    aBuku = BukuAdapter(this@HomePage, list)
                    recyclerView!!.adapter = aBuku
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                e(
                    "TAG_ERROR", p0.message
                )
            }

        })

        fAuth = FirebaseAuth.getInstance()

        logout.setOnClickListener {
            fAuth.signOut()
            pref.setStatusUser(false)
            pref.setStatus(false)
            Toast.makeText(
                this,
                "LOGOUT BERHASIL",
                Toast.LENGTH_SHORT
            ).show()
            startActivity(
                Intent(
                    this, MainActivity::class.java
                )
            )
        }

        fab.setOnClickListener {
            startActivity(
                Intent(
                    this, AddData::class.java
                )
            )
        }

    }

    override fun onUpdateData(bukuModel: BukuModel, position: Int) {
        val dataxxx = bukuModel.getKey()
        val intent = Intent(
            this@HomePage,
            AddData::class.java
        )
        intent.putExtra("kode", dataxxx)
        startActivity(intent)
    }

    override fun onDeleteData(bukuModel: BukuModel, position: Int) {
        dbRef = FirebaseDatabase.getInstance()
            .getReference("buku/${pref.getUid()}")
        dbRef.child(bukuModel.getKey()).removeValue()
            .addOnSuccessListener {
                Toast.makeText(
                    this,
                    "Delete Success",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

}