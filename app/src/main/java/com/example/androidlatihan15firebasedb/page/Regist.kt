package com.example.androidlatihan15firebasedb.page

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.androidlatihan15firebasedb.R
import com.example.androidlatihan15firebasedb.data.Pref
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.regis.*

class Regist : AppCompatActivity() {

    lateinit var fAuth: FirebaseAuth
    lateinit var dbRef: DatabaseReference
    lateinit var pref: Pref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.regis)

        fAuth = FirebaseAuth.getInstance()

        pref = Pref(this)

        bt_regis.setOnClickListener {
            val name = name_regis.text.toString()
            val email = email_regis.text.toString()
            val pass = pass_regis.text.toString()
            if (name.isNotEmpty() || email.isNotEmpty() || pass.isNotEmpty()) {
                fAuth.createUserWithEmailAndPassword(email, pass)
                    .addOnSuccessListener {
                        addUserToFB(name, email, pass)
                    }
                    .addOnFailureListener {
                        Toast.makeText(
                            this,
                            "Failed",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            }
        }
    }

    fun addUserToFB(name: String, email: String, pass: String) {
        dbRef = FirebaseDatabase.getInstance().getReference("user/${fAuth.currentUser?.uid}")
        dbRef.child("/name").setValue(name)
        dbRef.child("/email").setValue(email)
        dbRef.child("/pass").setValue(pass)
        pref.setStatusUser(true)
        Toast.makeText(
            this,
            "Register Successfull",
            Toast.LENGTH_SHORT
        ).show()

        startActivity(Intent(this, MainActivity::class.java))
    }

}