package com.example.androidlatihan15firebasedb.data.model

class UserModel {

    private var name: String? = null
    private var email: String? = null
    private var pass: String? = null

    constructor()

    constructor(name: String, email: String, pass: String) {
        this.name = name
        this.email = email
        this.pass = pass
    }

    fun getEmail(): String {
        return email!!
    }

    fun getName(): String {
        return name!!
    }

    fun getPass(): String {
        return pass!!
    }

    fun setEmail(email: String) {
        this.email = email
    }

    fun setName(name: String) {
        this.name = name
    }

    fun setPass(pass: String) {
        this.pass = pass
    }
}