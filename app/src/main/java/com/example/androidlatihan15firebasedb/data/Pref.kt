package com.example.androidlatihan15firebasedb.data

import android.content.Context
import android.content.SharedPreferences

class Pref {
    val USER_ID = "uidx"
    val ID_BUKU = "idbuku"
    val COUNTER_ID = "counter"
    val statusUserSlur = "STATUS_USER"
    val statusLogin = "STATUS"

    var mCtx: Context
    var sharedSet: SharedPreferences

    constructor(ctx: Context) {
        mCtx = ctx
        sharedSet = mCtx.getSharedPreferences(
            "APPTESTDB", Context.MODE_PRIVATE
        )
    }

    fun saveUid(uid: String) {
        val edit = sharedSet.edit()
        edit.putString(USER_ID, uid)
        edit.apply()
    }

    fun getUid(): String? {
        return sharedSet.getString(USER_ID, " ")
    }

    fun saveUidBook(uid: String) {
        val edit = sharedSet.edit()
        edit.putString(ID_BUKU, uid)
        edit.apply()
    }

    fun getUidBook(): String? {
        return sharedSet.getString(ID_BUKU, " ")
    }

    fun setStatusUser(statusUser: Boolean) {
        val edit = sharedSet.edit()
        edit.putBoolean(statusUserSlur, statusUser)
        edit.apply()
    }

    fun getStatusUser(): Boolean? {
        return sharedSet.getBoolean(statusUserSlur, false)
    }

    fun setStatus(status: Boolean) {
        val edit = sharedSet.edit()
        edit.putBoolean(statusLogin, status)
        edit.apply()
    }

    fun cekStatus(): Boolean? {
        return sharedSet.getBoolean(statusLogin, false)
    }

    fun saveCounterId(counter: Int) {
        val edit = sharedSet.edit()
        edit.putInt(COUNTER_ID, counter)
        edit.apply()
    }

    fun getCounterId(): Int {
        return sharedSet.getInt(COUNTER_ID, 1)
    }

}